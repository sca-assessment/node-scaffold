const _ = require('lodash');
const moment = require('moment');

// Function that simulates processing template input that includes user input
function processTemplate(userData) {
    // Vulnerable lodash template usage
    // Intentionally vulnerable to demonstrate the reachability of the code
    const compiled = _.template(`User data: <%- userData %>`);
    return compiled({ userData });
}

// Function to display formatted current date using moment
function displayCurrentDate() {
    return moment().format('YYYY-MM-DD HH:mm:ss');
}

// Simulating user input that includes potentially malicious content
const userInput = "'; console.log('Hacked!'); var nothing = '";

try {
    // Using moment to print the current date
    console.log('Current Date:', displayCurrentDate());

    // Process the user input with lodash template
    console.log(processTemplate(userInput));
} catch (error) {
    console.error('An error occurred:', error.message);
}